WITH RankedCustomers AS (
    SELECT
        s.cust_id,
        s.channel_id,
        SUM(s.amount_sold) AS total_sales,
        EXTRACT(YEAR FROM s.time_id) AS order_year,
        ROW_NUMBER() OVER (PARTITION BY s.channel_id, EXTRACT(YEAR FROM s.time_id) ORDER BY SUM(s.amount_sold) DESC) AS row_num
    FROM
        sh.sales s
    WHERE
        EXTRACT(YEAR FROM s.time_id) IN (1998, 1999, 2001)
    GROUP BY
        s.cust_id, s.channel_id, EXTRACT(YEAR FROM s.time_id)
)

SELECT
    cust_id,
    channel_id,
    order_year,
    ROUND(total_sales, 2) AS formatted_total_sales
FROM
    RankedCustomers
WHERE
    row_num <= 300
LIMIT 300;
